package com.kata.constants;

/**
 * class to hold constants
 *
 */
public class BuildTowerConstants {

	public static final String SPACES = " ";
	public static final String STAR = " *";
	public static final String NEW_LINE = "\n";
	
	private BuildTowerConstants() {
	}
}
