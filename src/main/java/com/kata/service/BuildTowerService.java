package com.kata.service;

import java.util.List;

/**
 * Service class which prints pyramid pattern based on input from user
 *
 */
public interface BuildTowerService {

	public List<String> buildPyramid(Integer noOfBlocks);
}
