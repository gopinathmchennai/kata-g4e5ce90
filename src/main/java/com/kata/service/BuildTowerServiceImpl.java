package com.kata.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kata.util.BuildTowerUtil;

/**
 * Service implementation class to build pyramids
 *
 */
@Service
public class BuildTowerServiceImpl implements BuildTowerService {
	
	@Autowired
	BuildTowerUtil buildTowerUtil;

	@Override
	public List<String>buildPyramid(Integer noOfBlocks) {
		return buildTowerUtil.buildPyramid(noOfBlocks);
	}

}
