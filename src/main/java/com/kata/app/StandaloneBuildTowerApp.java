package com.kata.app;

import java.io.InputStreamReader;
import java.util.Scanner;

import com.kata.util.BuildTowerUtil;

/**
 * Accepts input from user which says number of blocks for which pyramid should be built
 *
 */
public class StandaloneBuildTowerApp {
	
	public static void main(String args[]) {
		Scanner scanner = new Scanner(new InputStreamReader(System.in));
		System.out.println("Enter a number(building blocks for which you want to raise a pyramid) : ");
		try {
			Integer noOfBlocks = scanner.nextInt();
			BuildTowerUtil towerUtil = new BuildTowerUtil();
			towerUtil.buildPyramid(noOfBlocks);
		}
		catch(Exception e) {
			System.err.println("Invalid input - Check the number");
		}
		scanner.close();
		
	}

}
