
package com.kata.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.kata.service.BuildTowerService;

/**
 * Rest Controller - to test the pyramid pattern
 *
 */

@RestController
public class BuildTowerController {
	
	@Autowired
	BuildTowerService buildTowerService;

	 @ResponseBody
	 @RequestMapping(value = "/kata/build/pyramid", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<String> buildTower(@RequestParam Integer noOfBlocks) {
		 List<String> pyramidOutput = new ArrayList<String>();
		 try {
				if(null != noOfBlocks) {
					pyramidOutput = buildTowerService.buildPyramid(noOfBlocks);
				}
		 }
		 catch(Exception e) {
			 throw new ResponseStatusException( HttpStatus.BAD_REQUEST, "Invalid Input", e);	 
		 }
		
		return pyramidOutput;
	}
}
