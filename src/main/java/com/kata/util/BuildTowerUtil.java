/*
 * Copyright: This information constitutes the exclusive property of SEI
 * Investments Company, and constitutes the confidential and proprietary
 * information of SEI Investments Company.  The information shall not be
 * used or disclosed for any purpose without the written consent of SEI
 * Investments Company.
 */
package com.kata.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.kata.constants.BuildTowerConstants;

/**
 * Utility class or helper class to build pyramid pattern
 *
 */
@Component
public class BuildTowerUtil {

	/**
	 * This method is common to both standalone app and Rest Api.
	 * Will print in console for standalone app and store the pattern in string builder for REST API
	 * @param noOfBlocks
	 * @return
	 */
	public List<String> buildPyramid(Integer noOfBlocks) {
		List<String> pyramidPatterns = new ArrayList<String>();
		for(int i =0 ;i<noOfBlocks; i++) {
			StringBuilder pyramid =new StringBuilder();
			// to form pyramid structure, spaces at each level
			for(int k = 0; k <= noOfBlocks-i;k++) {
				System.out.print(BuildTowerConstants.SPACES);
				pyramid.append(BuildTowerConstants.SPACES);	
			}
			// No of stars to printed at each level
			for(int j = 0;j<=i; j++) {
				System.out.print(BuildTowerConstants.STAR);
				pyramid.append(BuildTowerConstants.STAR);
			}
			for(int k = 0; k <= noOfBlocks-i;k++) {
				System.out.print(BuildTowerConstants.SPACES);
				pyramid.append(BuildTowerConstants.SPACES);
			}
			pyramidPatterns.add(pyramid.toString());
			System.out.println(BuildTowerConstants.NEW_LINE);
		}
		return pyramidPatterns;

	}

}
