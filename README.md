Build Tower
===========
Purpose - Create pyramid shaped pattern using list of integers(corresponds to no of block) in Java

This Program will print pyramid shaped tower represented by * symbol.

Ways to test:
============

1) Start the application (KataApplication.Java) which will start the server in port 9092

2) Application exposes REST endpoint which you can use to test via any REST API TOOLS i.e. postman

Endpoint - http://localhost:9002/kata/build/pyramid <br/>
Http method - GET </br>
Query Parameter - noOfBlocks(for which you want to create a tower)<br/>

3) Response will print out the pyramid shaped pattern 

4) There is also standalone java class(StandaloneBuildTowerApp) which you can use to test the application which will print the shape in console. Program will accept integer as an input from user.

Output:
======
       *      
      * *     
     * * *    
    * * * *    
